package bootwildfly;

import bootwildfly.model.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRest {

    @RequestMapping("user")
    public User getUser() {
        User user = new User("Jakub", "Sokolowski");
        return user;
    }
}
